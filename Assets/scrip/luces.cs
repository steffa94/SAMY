﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class luces : MonoBehaviour{
    public Light miLuz1;
    public Light miLuz2;
    public Light miLuz3;
    public Light miLuz4;
    public Light miLuz5;
    public Light miLuz6;
    public Light miLuz7;
    public Light miLuz8;
    public Light miLuz9;
    public Light miLuz10;
    public Light miLuz11;
    public Light miLuz82;
    public Light miLuz12;
    public Light miLuz13;
    public Light miLuz14;
    public Light miLuz15;
    public Light miLuz16;
    public Light miLuz17;
    public Light miLuz18;
    public Light miLuz19;
    public Light miLuz20;
    public Light miLuz21;
    public Light miLuz22;
    public Light miLuz23;
    public Light miLuz24;
    public Light miLuz25;
    public Light miLuz26;

    public Light tubo1;
    public Light tubo1U;
    public Light tubo2;
    public Light tubo21;
    public Light tubo3;
    public Light tubo31;
    public Light tubo4;
    public Light tubo41;
    public Light tubo5;
    public Light tubo51;
    public Light tubo6;
    public Light tubo61;
    public Light tubo7;
    public Light tubo71;
    public Light tubo8;
    public Light tubo81;
    public Light tubo9;
    public Light tubo91;
    public Light tubo10;
    public Light tubo101;
    public Light tubo11;
    public Light tubo111;
    public Light tubo12;
    public Light tubo121;
    public Light tubo13;
    public Light tubo131;
    public Light tubo14;
    public Light tubo141;

    public Light capsula1;
    public Light capsula1U;
    public Light capsula2;
    public Light capsula21;
    public Light capsula3;
    public Light capsula31;
    public Light capsula4;
    public Light capsula41;
    public Light capsula5;
    public Light capsula51;
    public Light capsula6;
    public Light capsula61;
    public Light capsula7;
    public Light capsula71;

    public Light miLuz27;
    public Light miLuz28;
    public Light miLuz29;
    public Light miLuz30;
    public Light miLuz31;
    public Light miLuz32;

    public Light miLuz33;
    public Light miLuz34;
    public Light miLuz35;
    public Light miLuz36;
    public Light miLuz37;

    public Light miLuz38;
    public Light miLuz39;
    public Light miLuz40;
    public Light miLuz41;
    public Light miLuz42;
    public Light miLuz43;

    public Light cuadrostercero1;
    public Light cuadrostercero2;
    public Light cuadrostercero3;
    public Light cuadrostercero4;
    public Light cuadrostercero5;
    public Light cuadrostercero6;
    public Light cuadrostercero7;
    public Light cuadrostercero8;
    public Light cuadrostercero9;
    public Light cuadrostercero10;
    public Light cuadrostercero11;
    public Light cuadrostercero12;

    public Light lampara;
    public Light aplique1;
    public Light aplique2;
    public Light aplique3;
    public Light aplique4;
    public Light aplique5;
    public Light aplique6;
    public Light aplique7;
    public Light aplique8;
    public Light aplique9;

    public Light reflectorblanco1;
    public Light reflectorblanco2;
    public Light reflectorblanco3;
    public Light reflectorblanco4;
    public Light reflectorblanco5;
    public Light reflectorblanco6;
    public Light reflectorblanco7;

    public Light reflectorrojo1;
    public Light reflectorrojo2;
    public Light reflectorrojo3;
    public Light reflectorrojo4;

    public Light apliqueprimerpiso;
    public Light cuadrosprimero55;
    public Light cuadrosprimero56;
    public Light cuadrosprimero57;
    public Light cuadrosprimero58;
    public Light cuadrosprimero59;
    public Light cuadrosprimero60;

    public Light vitrinas44;
    public Light vitrinas45;
    public Light vitrinas46;
    public Light vitrinas47;
    public Light vitrinas48;
    public Light vitrinas49;
    public Light vitrinas50;
    public Light vitrinas51;
    public Light vitrinas52;
    public Light vitrinas53;
    public Light vitrinas54;
    public Light vitrinas61;
    public Light vitrinas62;
    public Light vitrinas63;
    public Light vitrinas64;
    public Light vitrinas65;
    public Light vitrinas66;
    public Light vitrinas67;

    public Light cuadrocuartopiso68;
    public Light cuadrocuartopiso69;
    public Light cuadrocuartopiso70;
    public Light cuadrocuartopiso71;
    public Light cuadrocuartopiso72;
    public Light cuadrocuartopiso73;
    public Light cuadrocuartopiso74;
    public Light cuadrocuartopiso75;
    public Light cuadrocuartopiso76;
    public Light cuadrocuartopiso77;
    public Light cuadrocuartopiso78;
    public Light cuadrocuartopiso79;
    public Light cuadrocuartopiso80;
    public Light cuadrocuartopiso1;
    public Light cuadrocuartopiso2;
    public Light cuadrocuartopiso3;
    public Light cuadrocuartopiso4;

    public Light alrededor1;
    public Light alrededor2;
    public Light alrededor3;
    public Light alrededor4;
    public Light alrededor5;
    public Light alrededor6;
    public Light alrededor7;
    public Light alrededor8;
    public Light alrededor9;
    public Light alrededor10;
    public Light alrededor11;
    public Light alrededor12;
    public Light alrededor13;
    public Light alrededor14;

    public Light postes;
    public Light postes1;
    public Light postes2;
    public Light postes3;
    public Light postes4;
    public Light postes5;
    public Light postes6;
    public Light postes7;




    // afuera en el pasillo 
    void Update() {

        if (Input.GetKeyDown("a")){
            miLuz1.enabled = !miLuz1.enabled;
            miLuz2.enabled = !miLuz2.enabled; 
            miLuz3.enabled = !miLuz3.enabled; 
            miLuz4.enabled = !miLuz4.enabled; 
            miLuz5.enabled = !miLuz5.enabled; 
            miLuz6.enabled = !miLuz6.enabled; 
            miLuz7.enabled = !miLuz7.enabled; 
            miLuz8.enabled = !miLuz8.enabled; 
            miLuz9.enabled = !miLuz9.enabled; 
            miLuz10.enabled = !miLuz10.enabled;
            miLuz11.enabled = !miLuz11.enabled;
            miLuz82.enabled = !miLuz82.enabled;
        }
        // planta baja
        if (Input.GetKeyDown("b"))
        {
            miLuz12.enabled = !miLuz12.enabled;
            miLuz13.enabled = !miLuz13.enabled;
            miLuz14.enabled = !miLuz14.enabled;
            miLuz15.enabled = !miLuz15.enabled;
            miLuz16.enabled = !miLuz16.enabled;
            miLuz17.enabled = !miLuz17.enabled;
            miLuz18.enabled = !miLuz18.enabled;
            miLuz19.enabled = !miLuz19.enabled;
            miLuz20.enabled = !miLuz20.enabled;
            miLuz21.enabled = !miLuz21.enabled;
            miLuz22.enabled = !miLuz22.enabled;
            miLuz23.enabled = !miLuz23.enabled;
            miLuz24.enabled = !miLuz24.enabled;
            miLuz25.enabled = !miLuz25.enabled;
            miLuz26.enabled = !miLuz26.enabled;
            }

        //  luces en los cuadros primer piso
        if (Input.GetKeyDown("c"))
        {
            apliqueprimerpiso.enabled = !apliqueprimerpiso.enabled;
            cuadrosprimero55.enabled = !cuadrosprimero55.enabled;
            cuadrosprimero56.enabled = !cuadrosprimero56.enabled;
            cuadrosprimero57.enabled = !cuadrosprimero57.enabled;
            cuadrosprimero58.enabled = !cuadrosprimero58.enabled;
            cuadrosprimero59.enabled = !cuadrosprimero59.enabled;
            cuadrosprimero60.enabled = !cuadrosprimero60.enabled;
        }

        // en los locasles 
        if (Input.GetKeyDown("d"))
        {
            tubo1.enabled = !tubo1.enabled;
            tubo1U.enabled = !tubo1U.enabled;
            tubo2.enabled = !tubo2.enabled;
            tubo21.enabled = !tubo21.enabled;
            tubo3.enabled = !tubo3.enabled;
            tubo31.enabled = !tubo31.enabled;
            tubo4.enabled = !tubo4.enabled;
            tubo41.enabled = !tubo41.enabled;
            tubo5.enabled = !tubo5.enabled;
            tubo51.enabled = !tubo51.enabled;
            tubo6.enabled = !tubo6.enabled;
            tubo61.enabled = !tubo61.enabled;
            tubo7.enabled = !tubo7.enabled;
            tubo71.enabled = !tubo71.enabled;
            tubo8.enabled = !tubo8.enabled;
            tubo81.enabled = !tubo81.enabled;
            tubo9.enabled = !tubo9.enabled;
            tubo91.enabled = !tubo91.enabled;
            tubo10.enabled = !tubo10.enabled;
            tubo101.enabled = !tubo101.enabled;
            tubo11.enabled = !tubo11.enabled;
            tubo111.enabled = !tubo111.enabled;
            tubo12.enabled = !tubo12.enabled;
            tubo121.enabled = !tubo121.enabled;
            tubo13.enabled = !tubo13.enabled;
            tubo131.enabled = !tubo131.enabled;
            tubo14.enabled = !tubo14.enabled;
            tubo141.enabled = !tubo141.enabled;
          }

        // SEGUNDO PAILLO DE CRISTAL
        if (Input.GetKeyDown("e")) { 
        capsula1.enabled = !capsula1.enabled;
        capsula1U.enabled = !capsula1U.enabled;
        capsula2.enabled = !capsula2.enabled;
        capsula21.enabled = !capsula21.enabled;
        capsula3.enabled = !capsula3.enabled;
        capsula31.enabled = !capsula31.enabled;
        capsula4.enabled = !capsula4.enabled;
        capsula41.enabled = !capsula41.enabled;
        capsula5.enabled = !capsula5.enabled;
        capsula51.enabled = !capsula51.enabled;
        capsula6.enabled = !capsula6.enabled;
        capsula61.enabled = !capsula61.enabled;
        capsula7.enabled = !capsula7.enabled;
        capsula71.enabled = !capsula71.enabled;
                    }
        // SEGUNDO Piso
        if (Input.GetKeyDown("f"))
        {
            miLuz27.enabled = !miLuz27.enabled;
            miLuz28.enabled = !miLuz28.enabled;
            miLuz29.enabled = !miLuz29.enabled;
            miLuz30.enabled = !miLuz30.enabled;
            miLuz31.enabled = !miLuz31.enabled;
            miLuz32.enabled = !miLuz32.enabled;
        }

        //  luces en vitrinas segundo piso
        if (Input.GetKeyDown("g"))
        {
            vitrinas44.enabled = !vitrinas44.enabled;
            vitrinas45.enabled = !vitrinas45.enabled;
            vitrinas46.enabled = !vitrinas46.enabled;
            vitrinas47.enabled = !vitrinas47.enabled;
            vitrinas48.enabled = !vitrinas48.enabled;
            vitrinas49.enabled = !vitrinas49.enabled;
            vitrinas50.enabled = !vitrinas50.enabled;
            vitrinas51.enabled = !vitrinas51.enabled;
            vitrinas52.enabled = !vitrinas52.enabled;
            vitrinas53.enabled = !vitrinas53.enabled;
            vitrinas54.enabled = !vitrinas54.enabled;
            vitrinas61.enabled = !vitrinas61.enabled;
            vitrinas62.enabled = !vitrinas62.enabled;
            vitrinas63.enabled = !vitrinas63.enabled;
            vitrinas64.enabled = !vitrinas64.enabled;
            vitrinas65.enabled = !vitrinas65.enabled;
            vitrinas66.enabled = !vitrinas66.enabled;
            vitrinas67.enabled = !vitrinas67.enabled;
        }

        //  tercer Piso
        if (Input.GetKeyDown("h"))
        {
            miLuz33.enabled = !miLuz33.enabled;
            miLuz34.enabled = !miLuz34.enabled;
            miLuz35.enabled = !miLuz35.enabled;
            miLuz36.enabled = !miLuz36.enabled;
            miLuz37.enabled = !miLuz37.enabled;
         }

        //  LUCES cuadros tercer piso 
        if (Input.GetKeyDown("i"))
        {
            cuadrostercero1.enabled = !cuadrostercero1.enabled;
            cuadrostercero2.enabled = !cuadrostercero2.enabled;
            cuadrostercero3.enabled = !cuadrostercero3.enabled;
            cuadrostercero4.enabled = !cuadrostercero4.enabled;
            cuadrostercero5.enabled = !cuadrostercero5.enabled;
            cuadrostercero6.enabled = !cuadrostercero6.enabled;
            cuadrostercero7.enabled = !cuadrostercero7.enabled;
            cuadrostercero8.enabled = !cuadrostercero8.enabled;
            cuadrostercero9.enabled = !cuadrostercero9.enabled;
            cuadrostercero10.enabled = !cuadrostercero10.enabled;
            cuadrostercero11.enabled = !cuadrostercero11.enabled;
            cuadrostercero12.enabled = !cuadrostercero12.enabled;

        }
        //  cuarto  Piso
        if (Input.GetKeyDown("j"))
        {
            miLuz38.enabled = !miLuz38.enabled;
            miLuz39.enabled = !miLuz39.enabled;
            miLuz40.enabled = !miLuz40.enabled;
            miLuz41.enabled = !miLuz41.enabled;
            miLuz42.enabled = !miLuz42.enabled;
            miLuz43.enabled = !miLuz43.enabled;
        }
        //  luces en cuadros cuarto piso
        if (Input.GetKeyDown("k"))
        {
            cuadrocuartopiso68.enabled = !cuadrocuartopiso68.enabled;
            cuadrocuartopiso69.enabled = !cuadrocuartopiso69.enabled;
            cuadrocuartopiso70.enabled = !cuadrocuartopiso70.enabled;
            cuadrocuartopiso71.enabled = !cuadrocuartopiso71.enabled;
            cuadrocuartopiso72.enabled = !cuadrocuartopiso72.enabled;
            cuadrocuartopiso73.enabled = !cuadrocuartopiso73.enabled;
            cuadrocuartopiso74.enabled = !cuadrocuartopiso74.enabled;
            cuadrocuartopiso75.enabled = !cuadrocuartopiso75.enabled;
            cuadrocuartopiso76.enabled = !cuadrocuartopiso76.enabled;
            cuadrocuartopiso77.enabled = !cuadrocuartopiso77.enabled;
            cuadrocuartopiso78.enabled = !cuadrocuartopiso78.enabled;
            cuadrocuartopiso79.enabled = !cuadrocuartopiso79.enabled;
            cuadrocuartopiso80.enabled = !cuadrocuartopiso80.enabled;
            cuadrocuartopiso1.enabled = !cuadrocuartopiso1.enabled;
            cuadrocuartopiso2.enabled = !cuadrocuartopiso2.enabled;
            cuadrocuartopiso3.enabled = !cuadrocuartopiso3.enabled;
            cuadrocuartopiso4.enabled = !cuadrocuartopiso4.enabled;

        }
               
        //  Apliques y lampara de la estatua
        if (Input.GetKeyDown("l"))
        {
            lampara.enabled = !lampara.enabled;
            aplique1.enabled = !aplique1.enabled;
            aplique2.enabled = !aplique2.enabled;
            aplique3.enabled = !aplique3.enabled;
            aplique4.enabled = !aplique4.enabled;
            aplique5.enabled = !aplique5.enabled;
            aplique6.enabled = !aplique6.enabled;
            aplique7.enabled = !aplique7.enabled;
            aplique8.enabled = !aplique8.enabled;
            aplique9.enabled = !aplique9.enabled;

        }

        //  reflectores  de la estatua y edificio
        if (Input.GetKeyDown("m"))
        {
            reflectorblanco1.enabled = !reflectorblanco1.enabled;
            reflectorblanco2.enabled = !reflectorblanco2.enabled;
            reflectorblanco3.enabled = !reflectorblanco3.enabled;
            reflectorblanco4.enabled = !reflectorblanco4.enabled;
            reflectorblanco5.enabled = !reflectorblanco5.enabled;
            reflectorblanco6.enabled = !reflectorblanco6.enabled;
            reflectorblanco7.enabled = !reflectorblanco7.enabled;
            reflectorrojo1.enabled = !reflectorrojo1.enabled;
            reflectorrojo2.enabled = !reflectorrojo2.enabled;
            reflectorrojo3.enabled = !reflectorrojo3.enabled;
            reflectorrojo4.enabled = !reflectorrojo4.enabled;

        }

        //  alrededor del edificio
        if (Input.GetKeyDown("n"))
        {
            alrededor1.enabled = !alrededor1.enabled;
            alrededor2.enabled = !alrededor2.enabled;
            alrededor3.enabled = !alrededor3.enabled;
            alrededor4.enabled = !alrededor4.enabled;
            alrededor5.enabled = !alrededor5.enabled;
            alrededor6.enabled = !alrededor6.enabled;
            alrededor7.enabled = !alrededor7.enabled;
            alrededor8.enabled = !alrededor8.enabled;
            alrededor9.enabled = !alrededor9.enabled;
            alrededor10.enabled = !alrededor10.enabled;
            alrededor11.enabled = !alrededor11.enabled;
            alrededor12.enabled = !alrededor12.enabled;
            alrededor13.enabled = !alrededor13.enabled;
            alrededor14.enabled = !alrededor14.enabled;

        }

        //  postes alrededor
        if (Input.GetKeyDown("o"))
        {
            postes.enabled = !postes.enabled;
            postes1.enabled = !postes1.enabled;
            postes2.enabled = !postes2.enabled;
            postes3.enabled = !postes3.enabled;
            postes4.enabled = !postes4.enabled;
            postes5.enabled = !postes5.enabled;
            postes6.enabled = !postes6.enabled;
            postes7.enabled = !postes7.enabled;
        }



    }
}
